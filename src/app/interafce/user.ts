import {Roles} from "../enum/Roles";
import {Book} from "./book";

export class User {
  id: number = 0;

  username: string = '';

  password: string = '';

  email: string = '';

  role: Roles = Roles.USER;

  userCode: string = '';

  cartList: Array<Book> = []

  constructor(){
  }
}
