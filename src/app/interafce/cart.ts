export class Cart {
  id: number = 0;

  id_user: number = 0;

  id_book: number = 0;

  constructor(id_user: number, id_book: number) {
    this.id_user = id_user;
    this.id_book = id_book;
  }
}
