export interface Book {
  id: number;

  title: string;

  author: string;

  ISBN: string;

  genre: string;

  price: number;

  pageSize: number;

  publishingHouse: string;

  releaseDate: string;

  index: number;

  originalLanguage: string;

  imageUrl: string;

  countSize: number;
}
