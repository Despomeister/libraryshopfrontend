import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookRoutingModule } from './book-routing.module';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { BookTableComponent } from './welcome-page/books-table/book-table.component';
import { FormsModule } from "@angular/forms";
import { UserService } from "../services/user.service";
import { LoginPanelComponent } from './welcome-page/login-panel/login-panel.component';
import {RegisterPanelComponent} from "./welcome-page/register-panel/register-panel.component";
import { UserPanelComponent } from './welcome-page/user-panel/user-panel.component';
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {Ng2OrderModule} from "ng2-order-pipe";
import {MatPaginatorModule} from "@angular/material/paginator";
import {NgxPaginationModule} from "ngx-pagination";


@NgModule({
  declarations: [
    WelcomePageComponent,
    BookTableComponent,
    LoginPanelComponent,
    RegisterPanelComponent,
    UserPanelComponent
  ],
    imports: [
        CommonModule,
        BookRoutingModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        FormsModule,
        MatTableModule,
        MatSortModule,
        Ng2OrderModule,
        MatPaginatorModule,
        NgxPaginationModule
    ],
  providers: [UserService],
})
export class BookModule { }
