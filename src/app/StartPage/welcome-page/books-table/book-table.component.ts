import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Book} from "../../../interafce/book";
import {HttpErrorResponse} from "@angular/common/http";
import {BookService} from "../../../services/book.service";
import {User} from "../../../interafce/user";
import {UserService} from "../../../services/user.service";
import {Sort} from "@angular/material/sort";
import {CartService} from "../../../services/cart.service";
import {Cart} from "../../../interafce/cart";

@Component({
  selector: 'app-book-table',
  templateUrl: './book-table.component.html',
  styleUrls: ['./book-table.component.css']
})
export class BookTableComponent implements OnChanges, OnInit {

  @Input() booksFromParent: Book[] = [];

  @Input() flagFromParent: boolean | undefined;

  public books: Book[] = [];

  public editBook: Book | undefined;

  isEnabled: boolean = false;

  counter: number = 0;

  title: string = "Enable edit book";

  public deleteBook: Book

  isDivVisible: boolean = false;

  loginFormFlag: boolean = true;

  successLoginFlag: boolean = false;

  key: string = 'id';

  reverse: boolean = false;

  users: User[] = [];

  user: User = new User();

  sortedBooks: Book[] = [];

  totalLength: number = 5;

  page: number = 1;

  cart: Cart = new Cart(0,0);

  constructor(private bookService: BookService, private userService: UserService, private cartService: CartService) {
    this.deleteBook = this.books[0];
    this.books = this.booksFromParent;
    this.sortedBooks = this.booksFromParent.slice();
  }

  public getBooks(): void {
    this.bookService.getBooks().subscribe(
      (response: Book[]) => {
        this.books = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getUsers(): void {
    this.userService.getUsers().subscribe(
      (response: User[]) => {
        this.users = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getBooksInCart(user: User): void {
    this.cartService.getBooksInCart(user.id).subscribe((response: Book[]) => {
        user.cartList = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getBooks();
    this.getUsers();
    this.isVisible(this.flagFromParent);
  }

  ngOnInit(): void {
    this.getUsers();
    this.totalLength = this.booksFromParent.length;
  }

  onEditBook(book: Book): void {
    this.bookService.updateBook(book).subscribe(
      () => {
        this.getBooks();
        this.bookService.filter('Update click')
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
    this.isEnabled = false;
    this.title = 'Enable edit book';
    this.counter++;
    if (this.counter === 2)
      this.counter = 0;
  }

  public onOpenModal(book: Book, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'delete') {
      this.deleteBook = book;
      button.setAttribute('data-target', '#deleteBookModal');
    }
    if (mode === 'edit') {
      this.editBook = book;
      button.setAttribute('data-target', '#showMoreInfoBookModal');
    }
    container?.appendChild(button);
    button.click();
  }

  public setFlag(counter: number) {
    if ((counter % 2) === 0) {
      this.isEnabled = true;
      this.title = 'Disable edit book';
    } else {
      this.title = 'Enable edit book';
      this.isEnabled = false;
    }
    this.counter++;
    if (this.counter === 2)
      this.counter = 0;
  }

  onDeleteBook(bookId: number): void {
    this.bookService.deleteBook(bookId).subscribe(
      (response: void) => {
        this.getBooks();
        this.extraModalClose();
        this.isDivVisible = this.books.length === 0;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      });
  }

  public extraModalClose(): void{
    const closeButton = document.getElementById('editBookFormButtonClose');
    closeButton?.click();
    this.bookService.filter('Delete click')
  }

  public isVisible(parent_flag?: boolean): boolean {

    if (parent_flag) {
      this.isDivVisible = parent_flag;
    }
    else this.isDivVisible = false;

    return this.isDivVisible;
  }

  flagChangedHandler($event: boolean): void {
    this.getUsers();
    this.loginFormFlag = $event;
  }

  isLogged($event: boolean): void {
    this.getUsers();
    this.successLoginFlag = $event;
  }

  setUser($event: User): void {
    this.getUsers();
    this.user = $event;
    this.user = this.users.filter(x => x.email == this.user.email)[0];
    this.getBooksInCart(this.user);
  }

  sortData(sort: Sort): void {
    const data = this.booksFromParent.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedBooks = data;
      this.booksFromParent = this.books;
      return;
    }

    this.sortedBooks = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'title': return compare(a.title, b.title, isAsc);
        case 'genre': return compare(a.genre, b.genre, isAsc);
        case 'releaseDate': return compare(a.releaseDate, b.releaseDate, isAsc);
        case 'originalLanguage': return compare(a.originalLanguage, b.originalLanguage, isAsc);
        case 'price': return compare(a.price, b.price, isAsc);
        default: return 0;
      }
    });
    this.booksFromParent = this.sortedBooks;
  }

  checkUser(book: Book, user: User): void {
    if (user.username !== '' && this.successLoginFlag) {
      this.addToBucketModal("logged", user, book);
    }
    else {
      this.addToBucketModal("notLogged", user, book);
    }
  }

  addToBucketModal(mode: string, user: User, book: Book): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'logged') {
      this.cart = new Cart(user.id,book.id);
      this.cartService.addToCart(this.cart).subscribe();
      button.setAttribute('data-target', '#userLoggedBucket');
    }
    if (mode === 'notLogged') {
      button.setAttribute('data-target', '#userNotLoggedBucket');
    }
    container?.appendChild(button);
    button.click();
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}


