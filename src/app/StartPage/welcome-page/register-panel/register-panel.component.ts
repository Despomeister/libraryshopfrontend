import {Component, EventEmitter, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {User} from "../../../interafce/user";
import {RegisterService} from "../../../services/register.service";
import {HttpErrorResponse} from "@angular/common/http";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-register-panel',
  templateUrl: './register-panel.component.html',
  styleUrls: ['./register-panel.component.css']
})
export class RegisterPanelComponent implements OnInit, OnChanges {

  @Output() goToLogin: EventEmitter<boolean> = new EventEmitter<any>();

  registerFlag: boolean = true;

  public users: User[] = [];

  user = new User();

  msg: string = '';

  registeredSuccess: boolean = false;

  accountExist: boolean = false;

  constructor(private registerService: RegisterService, private userService: UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getUsers();
  }

  public getUsers(): void {
    this.userService.getUsers().subscribe(
      (response: User[]) => {
        this.users = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public goToLoginFun(): void {
    this.registerFlag = true;
    this.goToLogin.emit(this.registerFlag);
  }

  registerUser() {
    const emailIsExists = this.users.find(element => element.email === this.user.email);
    const usernameIsExists = this.users.find(element => element.username === this.user.username);
    if (emailIsExists?.email === undefined && usernameIsExists?.username === undefined) {
      this.registerService.registerUserFromRemote(this.user).subscribe(
        data => {
          this.msg="Registration successful! Try to sign in.";
          this.accountExist = false;
          this.openModal();
          this.getUsers();
        },
        error => {
          this.msg=error.error;
        }
      )
    }
    else this.accountExist = true;
  }

  registerButtonClick() {
    this.registeredSuccess = true;
  }

  openModal() {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    button.setAttribute('data-target', '#registerSuccessModal');
    container?.appendChild(button);
    button.click();
  }
}
