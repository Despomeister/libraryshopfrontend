import {Component, DoCheck, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Book} from "../../interafce/book";
import {BookService} from "../../services/book.service";
import {HttpErrorResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit, DoCheck, OnChanges {

  public books: Book[] = [];

  public tempBooks: Book[] = [];

  public isVisible: boolean = false;

  results: Book[] = [];

  constructor(private bookService: BookService) {
    this.bookService.listen().subscribe(()=>{
      this.getBooks();
      this.isVisible = this.books.length === 0;
    })
  }

  public getBooks(): void {
    this.bookService.getBooks().subscribe(
      (response: Book[]) => {
        this.books = response;
        this.tempBooks = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  ngOnInit(): void {
    this.getBooks();
  }

  ngDoCheck() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  public onOpenModal(): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    button.setAttribute('data-target', '#addBookModal');
    container?.appendChild(button);
    button.click();
  }

  public onAddBook(addForm: NgForm): void {
    document.getElementById('addBookFormButtonClose')?.click();
    this.bookService.addBook(addForm.value).subscribe(
      (response: Book) => {
          this.getBooks();
      },
      (error: HttpErrorResponse) => {
          alert(error.message);
      }
    );
  }

  public searchBooks(key: string): void {
    this.results = [];
    if (key.length !== 0)
    {
      for (const book of this.tempBooks) {
        if (book.title.toLowerCase().indexOf(key.toLowerCase()) !== -1
          || book.genre.toLowerCase().indexOf(key.toLowerCase()) !== -1
          || book.publishingHouse.toLowerCase().indexOf(key.toLowerCase()) !== -1
          || book.price.toString().toLowerCase().indexOf(key.toLowerCase()) !== -1) {
          this.results.push(book);
        }
      }
      this.books = this.results;
    }
    else {
      this.getBooks();
    }

    if (this.results.length === 0 && !key) {
      this.isVisible = false;
    }
    else this.isVisible = this.results.length === 0;
  }
}
