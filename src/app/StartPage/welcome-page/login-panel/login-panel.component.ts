import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../interafce/user";
import {HttpErrorResponse} from "@angular/common/http";
import {LoginService} from "../../../services/login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-panel',
  templateUrl: './login-panel.component.html',
  styleUrls: ['./login-panel.component.css']
})
export class LoginPanelComponent implements OnInit {

  @Output() goToRegister: EventEmitter<boolean> = new EventEmitter<any>();

  @Output() goToUserPanel: EventEmitter<boolean> = new EventEmitter<any>();

  @Output() sendLoggedUser: EventEmitter<User> = new EventEmitter<User>();

  loginFlag: boolean = true;

  successFromLogin: boolean = false;

  public users: User[] = [];

  user = new User();

  msg: string = '';

  constructor(private userService: UserService, private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.getUsers();
  }

  public getUsers(): void {
    this.userService.getUsers().subscribe(
      (response: User[]) => {
        this.users = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public goToRegisterFun(): void {
    this.loginFlag = false;
    this.goToRegister.emit(this.loginFlag);
  }

  public loginSuccess(): void {
    this.successFromLogin = true;
    this.goToUserPanel.emit(this.successFromLogin);
  }

  loginUser() {
    this.sendLoggedUser.emit(this.user);
    this.loginService.loginUserFromRemote(this.user).subscribe(
      data =>{
        this.openModal();
      },
      error =>{
        this.msg = 'Bad email or password account, please try again.'
      });
  }

  openModal() {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    button.setAttribute('data-target', '#loginSuccessModal');
    container?.appendChild(button);
    button.click();
  }
}
