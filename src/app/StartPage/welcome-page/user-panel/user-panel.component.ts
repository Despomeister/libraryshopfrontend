import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../interafce/user";
import {HttpErrorResponse} from "@angular/common/http";
import {Book} from "../../../interafce/book";
import {CartService} from "../../../services/cart.service";

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit, OnChanges {

  public users: User[] = [];

  @Input() currentUser: User = new User();

  userDeleted: boolean = false;

  userLoggedOut: boolean = false;

  booksInCart: Array<Book> = [];

  headElements = ['#','','Title', 'Price', 'Count', ''];

  @Output() goToLoginFromUserPanel: EventEmitter<boolean> = new EventEmitter<any>();

  constructor(private userService: UserService, private cartService: CartService) {
    if (!this.currentUser.cartList) {
      this.booksInCart = [];
    }
  }

  ngOnInit(): void {
    this.getUsers();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getUsers();
  }

  public getUsers(): void {
    this.userService.getUsers().subscribe(
      (response: User[]) => {
        this.users = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  deleteUser(userId: number) {
    this.userDeleted = true;
    this.goToLoginFromUserPanel.emit(this.userDeleted);
    this.userService.deleteUser(userId).subscribe();
  }

  openDialog(mode: string, user?: User): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'delete') {
      button.setAttribute('data-target', '#deleteUserModal');
    }
    if (mode == 'logout') {
      button.setAttribute('data-target', '#logoutUserModal');
    }
    if (mode == 'success') {
      button.setAttribute('data-target', '#deleteSuccessModal');
    }
    if (mode == 'openCart') {
      this.cartService.getBooksInCart(this.currentUser.id).subscribe((response: Book[]) => {
          this.booksInCart = response;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
      button.setAttribute('data-target', '#openCartModal');
    }
    container?.appendChild(button);
    button.click();
  }

  logoutUser() {
    this.userLoggedOut = true;
    this.goToLoginFromUserPanel.emit(this.userLoggedOut);
  }

  getTotalCost() {
    return this.booksInCart?.map(t => t.price * t.countSize).reduce((acc, value) => acc + value, 0);
  }

  deleteFromCart(bookToRemove: Book, user: User) {
    this.cartService.deleteBookFromCart(bookToRemove.id, user.id).subscribe(
      (response: void) => {
        this.cartService.getBooksInCart(this.currentUser.id).subscribe((response: Book[]) => {
            this.booksInCart = response;
          },
          (error: HttpErrorResponse) => {
            alert(error.message);
          }
        );
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      });
  }
}
