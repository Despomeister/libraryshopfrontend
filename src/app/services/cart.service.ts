import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Cart} from "../interafce/cart";
import {Book} from "../interafce/book";

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  public addToCart(cart: Cart): Observable<Cart> {
    return this.http.post<Cart>(`${this.apiServerUrl}/cart/add`, cart)
}

  public getBooksInCart(userId: number): Observable<Book[]> {
    return this.http.get<Book[]>(`${this.apiServerUrl}/cart/${userId}`);
  }

  public deleteBookFromCart(bookId: number, userId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/cart/delete/${userId}/${bookId}`);
  }
}
