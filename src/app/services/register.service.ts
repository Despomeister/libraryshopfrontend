import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../interafce/user";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public registerUserFromRemote(user: User):Observable<any> {
    return this.http.post<any>(`${this.apiServerUrl}/user/add`, user);
  }
}
