import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import { Book } from "../interafce/book";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BookService {
  private apiServerUrl = environment.apiBaseUrl;

  private _books = new Subject<any>();

  constructor(private http: HttpClient) {
  }

  public getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(`${this.apiServerUrl}/book/all`);
  }

  public addBook(book: Book): Observable<Book> {
    return this.http.post<Book>(`${this.apiServerUrl}/book/add`, book);
  }

  public updateBook(book: Book): Observable<Book> {
    return this.http.put<Book>(`${this.apiServerUrl}/book/update`, book);
  }

  public deleteBook(bookId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/book/delete/${bookId}`);
  }

  public listen(): Observable<any>{
    return this._books.asObservable()
  }

  public filter(filterBy: string) {
    this._books.next(filterBy);
  }
}
