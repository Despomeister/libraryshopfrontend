import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { BookService } from "./services/book.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BookModule } from "./StartPage/book.module";
import { RegisterPanelComponent } from './StartPage/welcome-page/register-panel/register-panel.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BookModule
  ],
  providers:  [BookService],
  bootstrap: [AppComponent]
})
export class AppModule { }
